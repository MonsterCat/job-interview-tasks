/**
 * Тестовое задание - "Колода карт"
 * Количество выполненных задач - по желанию
 * 1) Реализовать классы Колоды и Карты
 * 2) Реализовать классы Руки игрока
 * 3) Реализовать Класс игры в карты
 * @author Alexey Lysenko
 */

class DeckType{
    constructor(name) {
        this.name = name;
    }
}
/**
 * Типы колоды пустая/36/52 карты
 * @type {DeckType[]}
 */
const deckTypes = [
    new DeckType('Empty'),
    new DeckType('Small'),
    new DeckType('Full'),
]

class Suit{
    constructor(name) {
        this.name = name;
    }
}
/**
 * Масти
 * @type {Suit[]}
 */
const suits = [
    new Suit('Clubs'),
    new Suit('Diamonds'),
    new Suit('Hearts'),
    new Suit('Spades')
]

class Rank {
    constructor(name) {
        this.name = name;
    }
}

/**
 * Достоинства, для упрощения задачи джокеров можно не делать
 * @type {Rank[]}
 */
const ranks = [
    new Rank('Ace'),
    new Rank('Two'),
    new Rank('Three'),
    new Rank('Four'),
    new Rank('Five'),
    new Rank('Six'),
    new Rank('Seven'),
    new Rank('Eight'),
    new Rank('Nine'),
    new Rank('Ten'),
    new Rank('Jack'),
    new Rank('Queen'),
    new Rank('King'),
]

/* ================  Задача 1 - реализовать классы Колоды и Карты   ================ */

/**
 * Колода создается сразу с картами (36 или 52)
 * В колоде должен быть контроль дублей
 */
class Deck {
    /**
     * @param {DeckType} type
     */
    constructor(type) {
        /**
         * @type {Card[]}
         */
       let cards = [];
    }
    /**
     * Перемешать колоду
     * @returns {Deck}
     */
    shuffle() {
    }

    /**
     * Вытянуть верхнюю карту
     * @returns {Card|boolean}
     */
    pullTop(){
    }

    /**
     * Вытянуть нижнюю карту
     * @returns {Card|boolean}
     */
    pullBottom(){
    }

    /**
     * Вытянуть случайную карту
     * @returns {Card|boolean}
     */
    pullRandom(){

    }
    /**
     * Вытануть карту нужной масти случайного достоинства
     * @param {Suit} suit
     * @returns {Card|boolean}
     */
    pullRandomSuit(suit){

    }

    /**
     * Вытянуть карту нужного достоинства случайной масти
     * @param {Rank} rank
     * @returns {Card|boolean}
     */
    pullRandomRank(rank) {

    }

    /**
     * Вытянуть нужную карту
     * @param {Suit} suit
     * @param {Rank} rank
     * @returns {Card|boolean}
     */
    pullExact(suit, rank){

    }

    /**
     * Положить карту в колоду.
     * @param {Card} card
     * @returns {Deck}
     */
    insertCard(card) {

    }
}

/**
 * Карта может быть одной из 4 мастей и одной из 13 достоинств
 */
class Card{
    /**
     *
     * @param {Suit} suit
     * @param {Rank} rank
     */
    constructor(suit, rank) {

    }
}


/* ================  Задача 2 - реализовать классы Руки игрока  ================ */


/**
 * Рука может быть пустой
 * Рука может взаимодействовать с колодой
 */
class PlayerHand extends Deck{
    /**
     * Вытянуть верхнюю карту из колоды в руку
     * @param {Deck} deck
     * @returns {Card|boolean}
     */
    pullTopFrom(deck){

    };

    /**
     * Вытянуть нижнюю карту из колоды в руку
     * @param {Deck} deck
     * @returns {Card|boolean}
     */
    pullBottomFrom(deck){

    }
    /**
     * Вытянуть случайную карту из колоды в руку
     * @param {Deck} deck
     * @returns {Card|boolean}
     */
    pullRandomFrom(deck){

    }
    /**
     * Вытануть карту нужной масти случайного достоинства из колоды в руку
     * @param {Deck} deck
     * @param {Suit} suit
     * @returns {Card|boolean}
     */
    pullRandomSuitFrom(deck, suit){

    }

    /**
     * Вытянуть карту нужного достоинства случайной масти из колоды в руку
     * @param {Deck} deck
     * @param {Rank} rank
     * @returns {Card|boolean}
     */
    pullRandomRankFrom(deck, rank) {

    }

    /**
     * Вытянуть нужную карту из колоды в руку
     * @param {Deck} deck
     * @param {Suit} suit
     * @param {Rank} rank
     * @returns {Card|boolean}
     */
    pullExactFrom(deck, suit, rank) {

    }
}


/* ================  Задача 3 - реализовать классы Игры ================ */


/**
 * В начале игры должна создаться колода, игроки, колода должна перемешаться, и каждый игрок должен получить карты на руки
 */
class Game {
    /**
     *
     * @param {DeckType} type
     * @param {number} startingHandSize
     * @param {number} numberOfPlayers
     */
    constructor(type, startingHandSize, numberOfPlayers) {
        this.type = type;
        this.startingHandSize = startingHandSize;
        this.numberOfPlayers = numberOfPlayers;
        /**
         * @property Deck раздача
         */
        this.hotDeck;
        /**
         * @property Deck отбой
         */
        this.wastedDeck;
        /**
         * @property {PlayerHand[]} игроки
         */
        this.hands;

    }
}
