/**
 * Тестовое задание - "Колода карт"
 * Количество выполненных задач - по желанию
 * 1) Реализовать классы Колоды и Карты
 * 2) Реализовать классы Руки игрока
 * 3) Реализовать Класс игры в карты
 * @author Alexey Lysenko
 */

//Типы колоды 36/52 карты
enum DeckType {
    Small,
    Full
}

//Масти
enum Suit {
    Clubs,
    Diamonds,
    Hearts,
    Spades
}

//Достоинства, для упрощения задачи джокеров можно не делать
enum Rank {
    Ace,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Jack,
    Queen,
    King
}

// Задача 1 - реализовать классы Колоды и Карты

interface CardInterface {
    suit: Suit;
    rank: Rank;
}

interface DeckInterface {

    cards: Array<CardInterface>;

    /**
     * Перемешать колоду
     */
    shuffle(): DeckInterface;

    /**
     * Вытянуть верхнюю карту
     */
    pullTop(): CardInterface;

    /**
     * Вытянуть нижнюю карту
     */
    pullBottom(): CardInterface & false;
    /**
     * Вытянуть случайную карту
     */
    pullRandom(): CardInterface & false;
    /**
     * Вытануть карту нужной масти случайного достоинства
     * @param suit
     */
    pullRandomSuit(suit: Suit): CardInterface & false;

    /**
     * Вытянуть карту нужного достоинства случайной масти
     * @param rank
     */
    pullRandomRank(rank: Rank): CardInterface & false;

    /**
     * Вытянуть нужную карту
     * @param suit
     * @param rank
     */
    pullExact(suit: Suit, rank: Rank): CardInterface & false;

    /**
     * Положить карту в колоду.
     * @param card
     */
    insertCard(card: CardInterface): DeckInterface

}

/**
 * Колода создается сразу с картами (36 или 52)
 * В колоде не должно быть дублей
 */
class Deck implements DeckInterface {
    constructor(type: DeckType) {

    }
}

/**
 * Карта может быть одной из 4 мастей и одной из 13 достоинств
 */
class Card implements CardInterface {
    constructor(suit: Suit, rank: Rank) {

    }
}


// Задача 2 - класс Рука


interface HandInterface extends DeckInterface {
    /**
     * Вытянуть верхнюю карту из колоды в руку
     */
    pullTopFrom(deck: DeckInterface): HandInterface;

    /**
     * Вытянуть нижнюю карту из колоды в руку
     */
    pullBottomFrom(deck: DeckInterface): HandInterface;
    /**
     * Вытянуть случайную карту колоды в руку
     */
    pullRandomFrom(deck: DeckInterface): HandInterface;
    /**
     * Вытануть карту нужной масти случайного достоинства из колоды в руку
     * @param deck
     * @param suit
     */
    pullRandomSuitFrom(deck: DeckInterface, suit: Suit): HandInterface;

    /**
     * Вытянуть карту нужного достоинства случайной масти из колоды в руку
     * @param deck
     * @param rank
     */
    pullRandomRankFrom(deck: DeckInterface, rank: Rank): HandInterface;

    /**
     * Вытянуть нужную карту из колоды в руку
     * @param deck
     * @param suit
     * @param rank
     */
    pullExactFrom(deck: DeckInterface, suit: Suit, rank: Rank): HandInterface;
}

/**
 * Рука может быть пустой
 * Рука может взаимодействовать с колодой
 */
class PlayerHand implements HandInterface {

}


// Задача 3 - реализовать Класс игры в карты
interface GameInterface {

    hotDeck: DeckInterface;
    wastedDeck: DeckInterface;

    hands: Array<PlayerHand>

}

/**
 * В начале игры должна создаться колода, игроки, колода должна перемешаться, и каждый игрок должен получить карты на руки
 */
class Game implements GameInterface {
    constructor(type: DeckType, startingHandSize: number, numberOfPlayers: number) {

    }
}
